<?php

use App\User;
use Illuminate\Database\Seeder;

class UserTabelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = now();
        $users = [
            [
                'name' => 'Admin',
                'email' => 'admin@blog.test',
                'password' => bcrypt('123123'),
                'created_at' => $now,
                'updated_at' => $now
            ]
        ];
        User::insert($users);
    }
}
