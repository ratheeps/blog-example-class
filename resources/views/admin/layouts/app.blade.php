<html>
<head>
    @include('admin.layouts.includes.meta')
    @include('admin.layouts.includes.styles')
</head>
<body>
<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
    @include('admin.layouts.includes.header')
    <div class="app-main">
        @include('admin.layouts.includes.side-bar')
        <div class="app-main__outer">
            <div class="app-main__inner">
                    @yield('content')
            </div>
            @include('admin.layouts.includes.footer')
        </div>
        <script src="http://maps.google.com/maps/api/js?sensor=true"></script>
    </div>
</div>
@include('admin.layouts.includes.scripts')
</body>
</html>
