<div class="form-row">
    <div class="form-group col-md-6">
        {!! Form::label('name', 'Name'); !!}
        {!! Form::text('name', null, ['class' =>[ 'form-control', $errors->has('name') ? 'is-invalid' : '' ]]); !!}
        <div class="invalid-feedback">
            {{ $errors->has('name') ?  $errors->first('name') : ''}}
        </div>
    </div>

    <div class="form-group col-md-6">
        {!! Form::label('email', 'Email'); !!}
        {!! Form::text('name', null, ['class' =>[ 'form-control', $errors->has('name') ? 'is-invalid' : '' ]]); !!}
        <div class="invalid-feedback">
            {{ $errors->has('name') ?  $errors->first('name') : ''}}
        </div>
    </div>
</div>

