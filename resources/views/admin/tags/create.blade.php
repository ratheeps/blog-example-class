@extends('admin.layouts.app')
@section('content')
    <div class="container">
        {!! Form::open(['route' => ['admin.tag.store'], 'method' => 'post']) !!}
            <div class="card">
                    <div class="card-body">
                        @include('admin.tags.form')
                    </div>
                    <div class="card-footer">
                        <a href="{{ route('admin.tag.index') }}" class="btn btn-dark mr-1">Cancel</a>
                        <button type="submit" class="btn btn-success">Create</button>
                    </div>
            </div>
        {!! Form::close() !!}
    </div>
@endsection
