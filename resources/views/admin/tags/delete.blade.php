@extends('admin.layouts.app')
@section('content')
    <div class="container">
        {!! Form::open(['route' => ['admin.tag.destroy', $tag->id], 'method' => 'delete']) !!}
            <div class="card">
                    <div class="card-body">
                        <div class="alert alert-danger">
                            Are you sure to delete {{ $tag->name }} ?
                        </div>
                    </div>
                    <div class="card-footer">
                        <a href="{{ route('admin.tag.index') }}" class="btn btn-dark mr-1">Cancel</a>
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </div>
            </div>
        {!! Form::close() !!}
    </div>
@endsection
