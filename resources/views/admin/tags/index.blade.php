@extends('admin.layouts.app')
@section('content')
    <div class="container">
            <div class="card">
                <div class="card-header">
                    <a href="{{ route('admin.tag.create') }}" class="btn btn-success">Create</a>
                </div>
                    <div class="card-body">
                        @if (session('success'))
                            <div class="alert alert-success">
                                {{ session('success') }}
                            </div>
                        @endif

                        <table class="table table-striped">
                            <tr>
                                <th>Name</th>
                                <th width="20%">Actions</th>
                            </tr>
                            @foreach($tags as $tag)
                                <tr>
                                    <td>{{ $tag->name }}</td>
                                    <td>
                                        <a href="{{ route('admin.tag.edit', ['tag' => $tag->id]) }}" class="btn btn-primary btn-sm">Edit</a>
                                        <a href="{{ route('admin.tag.delete',  ['tag' => $tag->id]) }}" class="btn btn-danger btn-sm">Delete</a>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                        {{ $tags->links() }}
                    </div>
            </div>
    </div>
@endsection
