<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware('auth')->prefix('admin')->namespace('Admin')->group(function (){
    Route::prefix('tags')->group(function (){
        Route::get('/', 'TagController@index')->name('admin.tag.index');
        Route::get('/create', 'TagController@create')->name('admin.tag.create');
        Route::post('/', 'TagController@store')->name('admin.tag.store');
        Route::prefix('{tag}')->group(function (){
            Route::get('/edit', 'TagController@edit')->name('admin.tag.edit');
            Route::get('/delete', 'TagController@delete')->name('admin.tag.delete');
            Route::patch('/', 'TagController@update')->name('admin.tag.update');
            Route::delete('/destroy', 'TagController@destroy')->name('admin.tag.destroy');
        });
    });
});
