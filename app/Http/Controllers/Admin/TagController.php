<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\TagRequest;
use App\Tag;
use Illuminate\Http\Request;

class TagController extends Controller
{
    public function index(){
        $tags = Tag::latest()->paginate(10);
        return view('admin.tags.index', compact('tags'));
    }

    public function create(){
        return view('admin.tags.create');
    }

    public function store(TagRequest $request){
        $tag = new Tag();
        $tag->setAttribute('name', $request->input('name'));
        $tag->save();
        return redirect()->route('admin.tag.index')->with(['success' => 'Tag successfully created !']);
    }

    public function edit(Tag $tag){
        return view('admin.tags.edit', compact('tag'));
    }

    public function update(TagRequest $request, Tag $tag){
        $tag->setAttribute('name', $request->input('name'));
        $tag->save();
        return redirect()->route('admin.tag.index')->with(['success' => 'Tag successfully updated!']);
    }

    public function delete(Tag $tag){
        return view('admin.tags.delete', compact('tag'));
    }

    public function destroy(Tag $tag){
        $tag->delete();
        return redirect()->route('admin.tag.index')->with(['success' => 'Tag successfully deleted!']);
    }
}
